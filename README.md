# Pi - OpenVPN Server

> [Link](https://dzone.com/articles/how-to-setup-an-openvpn-server-on-a-raspberry-pi)

## Download and Install Required Packages

`curl -L https://install.pivpn.io | bash`

### Assign Static IP Address

The VPN server requires a static IP to function correctly

### Select Your Default Gateway

